const KEY = 'LIKED_MOVIES_IDS';

export const likeMovie = (movieId: string): 'liked' | 'removedLike' => {
    const likedMovieIds = localStorage.getItem(KEY);
    if (likedMovieIds) {
        const ids = likedMovieIds.split(',');
        if (ids.includes(movieId)) {
            localStorage.setItem(
                KEY,
                ids.filter((id) => id !== movieId).toString()
            );

            return 'removedLike';
        }

        ids.push(movieId);
        localStorage.setItem(KEY, ids.toString());

        return 'liked';
    } else {
        localStorage.setItem(KEY, movieId.toString());

        return 'liked';
    }
};

export const isLikedMovie = (movieId: string) => {
    const likedMovieIds = localStorage.getItem(KEY);
    if (!likedMovieIds) return false;
    if (likedMovieIds.split(',').includes(movieId)) return true;
    else return false;
};

export const getLikedMoviesIds = () => {
    const likedMovieIds = localStorage.getItem(KEY);
    if (!likedMovieIds) return [];
    else return likedMovieIds.split(',');
};
