import { MovieDbMovie } from './api/movie-db/movies/types';
import { Movie } from './types';

export const mapMovieDbMoviesToMovies = (
    movieDbMovies: MovieDbMovie[]
): Movie[] => {
    return movieDbMovies.map((movie) => {
        return {
            id: movie.id.toString(),
            title: movie.title,
            overview: movie.overview,
            posterPath: movie.poster_path
                ? `https://image.tmdb.org/t/p/original${movie.poster_path}`
                : '',
            backdropPath: movie.poster_path
                ? `https://image.tmdb.org/t/p/original${movie.backdrop_path}`
                : '',
            releaseDate: movie.release_date,
        };
    });
};
