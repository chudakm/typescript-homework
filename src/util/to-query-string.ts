export const toQueryString = (
    data: Record<string, string | number | boolean>
): string => {
    const searchParams = new URLSearchParams();

    for (const key in data) {
        searchParams.append(key, String(data[key]));
    }

    return searchParams.toString();
};

export const getRandomInt = (min: number, max: number): number => {
    min = Math.ceil(min);
    max = Math.floor(max);

    return Math.floor(Math.random() * (max - min)) + min;
};
