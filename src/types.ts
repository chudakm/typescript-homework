export type MoviesSection = 'popular' | 'upcoming' | 'top-rated' | 'search';

export interface Movie {
    title: string;
    posterPath: string;
    overview: string;
    releaseDate: string;
    id: string;
    backdropPath: string;
}
