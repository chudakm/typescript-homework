import { mapMovieDbMoviesToMovies } from './api-mapper';
import { MovieDbApi } from './api/movie-db/movie-db';
import { MovieDbMovie } from './api/movie-db/movies/types';
import { getLikedMoviesIds } from './likes/likes';
import {
    removeFavoriteMovies,
    removeMovies,
    renderFavoriteMovies,
    renderMovies,
} from './render/movies-list';
import { renderRandomMovie } from './render/random-movie';
import { Movie, MoviesSection } from './types';
import { getRandomInt } from './util/to-query-string';

export async function render(): Promise<void> {
    let currentPage = 1;
    let moviesSection: MoviesSection = 'popular';
    let searchQuery = '';

    const { results: popularMovies } = await MovieDbApi.Movies.getPopular({
        page: currentPage,
    });

    const randomNumber = getRandomInt(0, popularMovies.length);
    const randomMovie = popularMovies[randomNumber];
    const [mappedRandomMovie] = mapMovieDbMoviesToMovies([randomMovie]);
    renderRandomMovie(mappedRandomMovie);

    renderMovies(mapMovieDbMoviesToMovies(popularMovies));

    const loadMoreButton = document.getElementById('load-more');
    if (loadMoreButton) {
        loadMoreButton.addEventListener('click', async () => {
            currentPage++;
            let movies: MovieDbMovie[];

            switch (moviesSection) {
                case 'popular':
                    const popularResponse = await MovieDbApi.Movies.getPopular({
                        page: currentPage,
                    });

                    movies = popularResponse.results;
                    break;
                case 'upcoming':
                    const upcomingResponse =
                        await MovieDbApi.Movies.getUpcoming({
                            page: currentPage,
                        });

                    movies = upcomingResponse.results;
                    break;
                case 'top-rated':
                    const topRatedResponse =
                        await MovieDbApi.Movies.getTopRated({
                            page: currentPage,
                        });

                    movies = topRatedResponse.results;
                    break;
                case 'search':
                    const searchResponse = await MovieDbApi.Search.searchMovies(
                        {
                            page: currentPage,
                            query: searchQuery,
                        }
                    );

                    movies = searchResponse.results;
                    break;
            }

            renderMovies(mapMovieDbMoviesToMovies(movies));
        });
    }

    const addMovieSectionsButtonsEventListeners = (
        section: Exclude<MoviesSection, 'search'>,
        id: string
    ) => {
        const button = document.getElementById(id);
        if (button) {
            button.addEventListener('click', async () => {
                if (moviesSection === section) return;

                moviesSection = section;
                currentPage = 1;

                removeMovies();
                let movies: MovieDbMovie[];

                switch (section) {
                    case 'popular':
                        const popular = await MovieDbApi.Movies.getPopular({
                            page: currentPage,
                        });

                        movies = popular.results;
                        break;
                    case 'top-rated':
                        const topRated = await MovieDbApi.Movies.getTopRated({
                            page: currentPage,
                        });

                        movies = topRated.results;
                        break;
                    case 'upcoming':
                        const upcoming = await MovieDbApi.Movies.getUpcoming({
                            page: currentPage,
                        });

                        movies = upcoming.results;
                        break;
                }

                renderMovies(mapMovieDbMoviesToMovies(movies));
            });
        }
    };

    addMovieSectionsButtonsEventListeners('popular', 'popular');
    addMovieSectionsButtonsEventListeners('upcoming', 'upcoming');
    addMovieSectionsButtonsEventListeners('top-rated', 'top_rated');

    const searchButton = document.getElementById('submit');
    if (searchButton) {
        searchButton.addEventListener('click', async () => {
            currentPage = 1;
            moviesSection = 'search';

            removeMovies();

            const search = (<HTMLInputElement>document.getElementById('search'))
                .value;
            if (search) {
                searchQuery = search;

                const { results } = await MovieDbApi.Search.searchMovies({
                    query: search,
                    page: currentPage,
                });

                renderMovies(mapMovieDbMoviesToMovies(results));
            }
        });
    }

    const favoriteMoviesButton = document.getElementById('navbar-toggler');
    favoriteMoviesButton?.addEventListener('click', async () => {
        const likedMovieIds = getLikedMoviesIds();
        const movies: Movie[] = [];

        for (const movieId of likedMovieIds) {
            const movie = await MovieDbApi.Movies.getDetails(Number(movieId));
            const [mappedMovie] = mapMovieDbMoviesToMovies([movie]);

            movies.push(mappedMovie);
        }

        renderFavoriteMovies(movies);
    });

    const closeFavoriteMoviesButton = document.getElementById('close-favorite');
    closeFavoriteMoviesButton?.addEventListener('click', () => {
        removeFavoriteMovies();
    });
}
