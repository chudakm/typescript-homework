export interface MovieDbMovie {
    poster_path?: string;
    adult: boolean;
    overview: string;
    release_date: string;
    genre_ids: number[];
    id: number;
    original_title: string;
    original_language: string;
    title: string;
    backdrop_path?: string;
    popularity: number;
    vote_count: number;
    video: boolean;
    vote_average: number;
}

export interface GetDetailsParams {
    language?: string;
    append_to_response?: string;
}

export interface GetDetailsResponse extends MovieDbMovie {}

export interface GetPopularParams {
    language?: string;
    page?: number;
    region?: string;
}

export interface GetPopularResponse {
    page: number;
    results: MovieDbMovie[];
    total_results: number;
    total_pages: number;
}

export interface GetTopRatedParams {
    language?: string;
    page?: number;
    region?: string;
}

export interface GetTopRatedResponse {
    page: number;
    results: MovieDbMovie[];
    total_results: number;
    total_pages: number;
}

export interface GetUpcomingParams {
    language?: string;
    page?: number;
    region?: string;
}

export interface GetUpcomingResponse {
    page: number;
    results: MovieDbMovie[];
    dates: {
        maximum: string;
        minimum: string;
    };
    total_results: number;
    total_pages: number;
}
