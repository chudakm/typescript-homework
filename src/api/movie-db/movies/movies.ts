import { toQueryString } from '../../../util/to-query-string';
import {
    GetDetailsParams,
    GetDetailsResponse,
    GetPopularParams,
    GetPopularResponse,
    GetTopRatedParams,
    GetTopRatedResponse,
    GetUpcomingParams,
    GetUpcomingResponse,
} from './types';

export class Movies {
    private prefix: string = 'movie/';

    constructor(private apiKey: string, private baseUrl: string) {}

    public async getDetails(
        movieId: number,
        params?: GetDetailsParams
    ): Promise<GetDetailsResponse> {
        const url = this.baseUrl + this.prefix + `${movieId}?`;
        const queryString = toQueryString({ api_key: this.apiKey, ...params });
        const fetchOptions = {
            method: 'GET',
        };

        const response = await fetch(url + queryString, fetchOptions);
        return response.json();
    }

    public async getPopular(
        params?: GetPopularParams
    ): Promise<GetPopularResponse> {
        const url = this.baseUrl + this.prefix + 'popular?';
        const queryString = toQueryString({ api_key: this.apiKey, ...params });
        const fetchOptions = {
            method: 'GET',
        };

        const response = await fetch(url + queryString, fetchOptions);
        return response.json();
    }

    public async getTopRated(
        params?: GetTopRatedParams
    ): Promise<GetTopRatedResponse> {
        const url = this.baseUrl + this.prefix + 'top_rated?';
        const queryString = toQueryString({ api_key: this.apiKey, ...params });
        const fetchOptions = {
            method: 'GET',
        };

        const response = await fetch(url + queryString, fetchOptions);
        return response.json();
    }

    public async getUpcoming(
        params?: GetUpcomingParams
    ): Promise<GetUpcomingResponse> {
        const url = this.baseUrl + this.prefix + 'upcoming?';
        const queryString = toQueryString({ api_key: this.apiKey, ...params });
        const fetchOptions = {
            method: 'GET',
        };

        const response = await fetch(url + queryString, fetchOptions);
        return response.json();
    }
}
