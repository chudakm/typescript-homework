import { Movies } from './movies/movies';
import { Search } from './search/search';

export class MovieDbApi {
    private static apiKey: string = '995b9042e9fb6798fd9f1056c0ebbd20';
    private static baseUrl: string = 'https://api.themoviedb.org/3/';

    public static Search = new Search(MovieDbApi.apiKey, MovieDbApi.baseUrl);
    public static Movies = new Movies(MovieDbApi.apiKey, MovieDbApi.baseUrl);
}
