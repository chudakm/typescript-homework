import { MovieDbMovie } from '../movies/types';

export interface SearchMoviesParams {
    language?: string;
    query: string;
    page?: number;
    include_adult?: boolean;
    region?: string;
    year?: number;
    primary_release_year?: number;
}

export interface SearchMoviesResponse {
    page: number;
    results: MovieDbMovie[];
    total_results: number;
    total_pages: number;
}
