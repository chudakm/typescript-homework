import { toQueryString } from '../../../util/to-query-string';
import { SearchMoviesParams, SearchMoviesResponse } from './types';

export class Search {
    private prefix: string = 'search/';

    constructor(private apiKey: string, private baseUrl: string) {}

    public async searchMovies(
        params: SearchMoviesParams
    ): Promise<SearchMoviesResponse> {
        const url = this.baseUrl + this.prefix + `movie?`;
        const queryString = toQueryString({
            api_key: this.apiKey,
            ...params,
            query: encodeURIComponent(params.query),
        });
        const fetchOptions = {
            method: 'GET',
        };

        const response = await fetch(url + queryString, fetchOptions);
        return response.json();
    }
}
