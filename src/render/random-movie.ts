import { Movie } from '../types';

export const renderRandomMovie = (movie: Movie) => {
    const randomMovieElement = document.getElementById('random-movie');
    if (randomMovieElement && movie.backdropPath) {
        randomMovieElement.style.backgroundImage = `url(${movie.backdropPath})`;
        randomMovieElement.style.backgroundSize = 'cover';
    }

    const nameElement = document.getElementById('random-movie-name');
    if (nameElement) {
        nameElement.textContent = movie.title;
    }

    const descriptionElement = document.getElementById(
        'random-movie-description'
    );
    if (descriptionElement) {
        descriptionElement.textContent = movie.overview;
    }
};
