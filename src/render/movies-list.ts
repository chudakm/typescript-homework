import { isLikedMovie, likeMovie } from '../likes/likes';
import { Movie } from '../types';

export const renderMovies = (movies: Movie[]) => {
    movies.forEach((movie) => renderMovie(movie));
};

const renderMovie = (movie: Movie) => {
    const isLiked = isLikedMovie(movie.id.toString());

    const movieElement = document.createElement('div');
    movieElement.className = 'col-lg-3 col-md-4 col-12 p-2';
    const cardElement = createCardElement({ ...movie, isLiked });

    movieElement.appendChild(cardElement);

    const filmContainer = document.getElementById('film-container');
    filmContainer?.appendChild(movieElement);
};

export const renderFavoriteMovies = (movies: Movie[]) => {
    movies.forEach((movie) => renderFavoriteMovie(movie));
};

export const renderFavoriteMovie = (movie: Movie) => {
    const movieElement = document.createElement('div');
    movieElement.className = 'col-12 p-2';

    const cardElement = createCardElement({ ...movie, isLiked: true });

    movieElement.appendChild(cardElement);

    const filmContainer = document.getElementById('favorite-movies');
    filmContainer?.appendChild(movieElement);
};

export const removeMovies = () => {
    const filmContainerElement = document.getElementById('film-container');
    if (filmContainerElement) removeAllChildsOfElement(filmContainerElement);
};

export const removeFavoriteMovies = () => {
    const favoriteContainer = document.getElementById('favorite-movies');
    if (favoriteContainer) removeAllChildsOfElement(favoriteContainer);
};

const createCardElement: (
    params: { isLiked: boolean } & Movie
) => HTMLDivElement = ({
    id: movieId,
    isLiked,
    posterPath,
    overview,
    releaseDate,
}) => {
    const cardElement = document.createElement('div');
    cardElement.className = 'card shadow-sm';

    const movieImgElement = document.createElement('img');
    movieImgElement.src = posterPath;

    const heartSvgElement = createHeartElement(
        isLiked,
        getIdForHeartElement(movieId)
    );
    heartSvgElement.onclick = () => {
        const status = likeMovie(movieId);

        const heartSvgElements = document.querySelectorAll(
            `#${getIdForHeartElement(movieId)}`
        );
        heartSvgElements.forEach((heartSvgElement) => {
            if (status === 'liked')
                (<SVGElement>heartSvgElement).style.fill = 'red';
            else (<SVGElement>heartSvgElement).style.fill = '#ff000078';
        });
    };

    const cardBodyElement = document.createElement('div');
    cardBodyElement.className = 'card-body';

    const cardTextElement = document.createElement('p');
    cardTextElement.className = 'card-text truncate';

    const cardText = document.createTextNode(overview);

    const commentWrapperElement = document.createElement('div');
    commentWrapperElement.className = `  d-flex
  justify-content-between
  align-items-center`;

    const smallElement = document.createElement('small');
    smallElement.className = 'text-muted';

    const smallText = document.createTextNode(releaseDate);

    cardBodyElement.appendChild(cardTextElement);
    cardTextElement.appendChild(cardText);

    cardBodyElement.appendChild(commentWrapperElement);
    commentWrapperElement.appendChild(smallElement);
    smallElement.appendChild(smallText);

    cardElement.appendChild(movieImgElement);
    cardElement.appendChild(heartSvgElement);
    cardElement.appendChild(cardBodyElement);

    return cardElement;
};

const createHeartElement = (isLiked: boolean, id?: string) => {
    const heartSvgElement = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'svg'
    );
    heartSvgElement.setAttributeNS(
        null,
        'class',
        'bi bi-heart-fill position-absolute p-2'
    );
    if (id) heartSvgElement.setAttributeNS(null, 'id', id);
    heartSvgElement.setAttributeNS(null, 'stroke', 'red');
    heartSvgElement.setAttributeNS(null, 'fill', isLiked ? 'red' : '#ff000078');
    heartSvgElement.setAttributeNS(null, 'width', '50');
    heartSvgElement.setAttributeNS(null, 'height', '50');
    heartSvgElement.setAttributeNS(null, 'viewBox', '0 -2 18 22');

    const heartSvgPathElement = document.createElementNS(
        'http://www.w3.org/2000/svg',
        'path'
    );
    heartSvgPathElement.setAttributeNS(null, 'fill-rule', 'evenodd');
    heartSvgPathElement.setAttributeNS(
        null,
        'd',
        'M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z'
    );

    heartSvgElement.appendChild(heartSvgPathElement);

    return heartSvgElement;
};

const getIdForHeartElement = (movieId: string) => `movie${movieId}`;
const removeAllChildsOfElement = (element: HTMLElement) => {
    while (element.lastChild) element.removeChild(element.lastChild);
};
